package eu.meikas.kristel.measurementcollection.controller;

import eu.meikas.kristel.measurementcollection.dao.ApartmentDAO;
import eu.meikas.kristel.measurementcollection.dao.MeasurementsDAO;
import eu.meikas.kristel.measurementcollection.domain.Apartment;
import eu.meikas.kristel.measurementcollection.domain.Measurements;
import eu.meikas.kristel.measurementcollection.helper.Report;
import eu.meikas.kristel.measurementcollection.helper.WaterConsumption;
import eu.meikas.kristel.measurementcollection.service.ApartmentUserDetails;
import eu.meikas.kristel.measurementcollection.util.LoginUtil;
import eu.meikas.kristel.measurementcollection.validator.MeasurementsValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.LocalDate;
import java.util.List;

@Controller
public class MeasurementsController {

	@Autowired
	ApartmentDAO apartmentDAO;
	@Autowired
	MeasurementsDAO measurementsDAO;

	@ModelAttribute("loginApartment")
	public Apartment getApartment() {
		return new Apartment();
	}

	@ModelAttribute("allApartments")
	public List<String> getAllApartments() {
		return apartmentDAO.getAllApartmentNames();
	}

	@ModelAttribute("latestMeasurements")
	public List<Measurements> getLatestMeasurementsForApartment() {
		ApartmentUserDetails userDetails = LoginUtil.getLoggedInApartmentDetails();
		if (userDetails != null) {
			return measurementsDAO.getPreviousMeasurementsByApartment(userDetails.getId());
		} else {
			return null;
		}
	}

	@ModelAttribute("measurementYears")
	public int[] getAllMeasurementYears() {
		return new int[]{2015, 2016, 2017};
	}

	@ModelAttribute("measurementMonths")
	public int[] getAllMeasurementMonths() {
		return new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
	}

	@ModelAttribute("newMeasurements")
	public Measurements createNewMeasurements() {
		List<Measurements> latestMeasurements = getLatestMeasurementsForApartment();
		if (latestMeasurements != null && !latestMeasurements.isEmpty()) {
			Measurements prefilledMeasurements = latestMeasurements.get(0);
			prefilledMeasurements.setYear(LocalDate.now().getYear());
			prefilledMeasurements.setMonth(LocalDate.now().getMonthValue());
			return prefilledMeasurements;
		} else {
			return new Measurements();
		}
	}

	@RequestMapping(value = {"", "/", "/index"}, method = RequestMethod.GET)
	public String showIndex() {
		return "index";
	}

	@RequestMapping(value = {"", "/", "/index"}, params = {"login"}, method = RequestMethod.POST)
	public String authenticateApartment(@ModelAttribute("loginApartment") Apartment apartment, BindingResult bindingResult) {
			return "index";
	}

	@RequestMapping(value = "/measurements", method = RequestMethod.GET)
	public String showMeasurementEntryForm() {
		return "measurements";
	}

	@RequestMapping(value = "/measurements", params = {"enterMeasurements"}, method = RequestMethod.POST)
	public String createMeasurementEntry(@ModelAttribute("newMeasurements") Measurements measurements,
										 BindingResult result, final Model model) {
		MeasurementsValidator validator = new MeasurementsValidator();
		validator.setMeasurementsDAO(measurementsDAO);
		ApartmentUserDetails userDetails = LoginUtil.getLoggedInApartmentDetails();
		if (userDetails != null) {
			measurements.setApartmentId(userDetails.getId());
		}

		validator.validate(measurements, result);
		if (result.hasErrors()) {
		} else {
			model.addAttribute("waterConsumption", new WaterConsumption(userDetails.getUsername(), measurements,
					measurementsDAO.getPreviousMeasurementsWithoutNulls(userDetails.getId(), measurements.getMeasurementPeriod())));
			measurementsDAO.insertNewMeasurements(measurements);
		}
		return "measurements";
	}

	@RequestMapping(value = "/report")
	public String showReportPage(final Model model) {
		Report report = new Report(measurementsDAO);
		model.addAttribute("report", report);
		return "report";
	}

	@RequestMapping(value = "/report", params = {"selectPeriod"}, method = RequestMethod.POST)
	public String querySelectedPeriod(@ModelAttribute("report") Report report, BindingResult result, final Model model) {
		if (report.getMeasurementsDAO() == null)
			report.setMeasurementsDAO(measurementsDAO);
		model.addAttribute("waterConsumptionList", report.getPeriodConsumption(getAllApartments()));
		return "report";
	}
}
