package eu.meikas.kristel.measurementcollection.domain;

public class Apartment {

	private int id = -1;		// id
	private String username;		// name
	private String password;
	private String passhash;	// passhash
	private String salt;		// salt - not used in this implementation
	private boolean isManager;	// manager
	private boolean isAuthenticated = false;

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasshash() {
		return this.passhash;
	}

	public void setPasshash(String passhash) {
		this.passhash = passhash;
	}

	public String getSalt() {
		return this.salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public boolean isManager() {
		return this.isManager;
	}

	public void setManager(boolean manager) {
		this.isManager = manager;
	}

	public boolean isAuthenticated() {
		return this.isAuthenticated;
	}

	public void setAuthenticated(boolean authenticated) {
		this.isAuthenticated = authenticated;
	}

	@Override
	public String toString() {
		return "Apartment{" +
				"id=" + id +
				", username='" + username + '\'' +
				", password='" + password + '\'' +
				", passhash='" + passhash + '\'' +
				", salt='" + salt + '\'' +
				", isManager=" + isManager +
				", isAuthenticated=" + isAuthenticated +
				'}';
	}
}
