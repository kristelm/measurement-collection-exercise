package eu.meikas.kristel.measurementcollection.domain;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Measurements {
	private final int ROUNDING_MODE = BigDecimal.ROUND_CEILING;

	private int id;							// id
	private int apartmentId;				// apartment_id
	private String apartmentName;
	private BigDecimal kitchenHot;				// kitchen_hot
	private BigDecimal kitchenCold;				// kitchen_cold
	private BigDecimal bathroomHot;				// bathroom_hot
	private BigDecimal bathroomCold;			// bathroom_cold
	private LocalDate measurementPeriod;	// measurement_period
	// Date date = Date.valueOf(localDate); <-> LocalDate localDate = date.toLocalDate();

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getApartmentId() {
		return this.apartmentId;
	}

	public void setApartmentId(int apartmentId) {
		this.apartmentId = apartmentId;
	}

	public String getApartmentName() {
		return this.apartmentName;
	}

	public void setApartmentName(String apartmentName) {
		this.apartmentName = apartmentName;
	}

	public int getYear() {
		if (this.measurementPeriod != null)
			return this.measurementPeriod.getYear();
		else
			return -1;
	}

	public void setYear(int year) {
		if (this.measurementPeriod == null)
			this.measurementPeriod = LocalDate.now();
		this.measurementPeriod = this.measurementPeriod.withYear(year).withDayOfMonth(1);
	}

	public int getMonth() {
		if (this.measurementPeriod != null)
			return this.measurementPeriod.getMonthValue();
		else
			return -1;
	}

	public void setMonth(int month) {
		if (this.measurementPeriod == null)
			this.measurementPeriod = LocalDate.now();
		this.measurementPeriod = this.measurementPeriod.withMonth(month).withDayOfMonth(1);
	}

	public BigDecimal getKitchenHot() {
		return (this.kitchenHot == null) ? null : this.kitchenHot.setScale(3, ROUNDING_MODE);
	}

	public void setKitchenHot(BigDecimal kitchenHot) {
		this.kitchenHot = kitchenHot;
	}

	public BigDecimal getKitchenCold() {
		return (this.kitchenCold == null) ? null : this.kitchenCold.setScale(3, ROUNDING_MODE);
	}

	public void setKitchenCold(BigDecimal kitchenCold) {
		this.kitchenCold = kitchenCold;
	}

	public BigDecimal getBathroomHot() {
		return (this.bathroomHot == null) ? null : this.bathroomHot.setScale(3, ROUNDING_MODE);
	}

	public void setBathroomHot(BigDecimal bathroomHot) {
		this.bathroomHot = bathroomHot;
	}

	public BigDecimal getBathroomCold() {
		return (this.bathroomCold == null) ? null : this.bathroomCold.setScale(3, ROUNDING_MODE);
	}

	public void setBathroomCold(BigDecimal bathroomCold) {
		this.bathroomCold = bathroomCold;
	}

	public LocalDate getMeasurementPeriod() {
		return this.measurementPeriod;
	}

	public void setMeasurementPeriod(LocalDate measurementPeriod) {
		this.measurementPeriod = measurementPeriod.withDayOfMonth(1);
	}

	@Override
	public String toString() {
		return "Measurements{" +
				"id=" + id +
				", apartmentId=" + apartmentId +
				", apartmentName='" + apartmentName + '\'' +
				", kitchenHot=" + kitchenHot +
				", kitchenCold=" + kitchenCold +
				", bathroomHot=" + bathroomHot +
				", bathroomCold=" + bathroomCold +
				", measurementPeriod=" + measurementPeriod +
				'}';
	}
}
