package eu.meikas.kristel.measurementcollection;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeasurementCollectionApplication implements CommandLineRunner{


	public static void main(String[] args) throws Throwable {
		SpringApplication.run(MeasurementCollectionApplication.class, args);
	}

	public void run(String... strings) throws Exception {
	}
}
