package eu.meikas.kristel.measurementcollection.helper;

import eu.meikas.kristel.measurementcollection.dao.MeasurementsDAO;
import eu.meikas.kristel.measurementcollection.domain.Measurements;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class Report {
	private int year;
	private int month;
	private List<WaterConsumption> periodConsumption;

	private MeasurementsDAO measurementsDAO;

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public List<WaterConsumption> getPeriodConsumption() {
		return periodConsumption;
	}

	public List<WaterConsumption> getPeriodConsumption(List<String> apartmentNames) {
		LinkedHashMap<String, Measurements> allApartmentMeasurements = new LinkedHashMap<String, Measurements>(25, 0.75f, false);
		for (String name : apartmentNames)
			allApartmentMeasurements.put(name, null);
		List<Measurements> periodMeasurements = this.measurementsDAO.getAllMeasurementsByPeriod(this.year, this.month);
		LocalDate reportPeriod = LocalDate.parse(String.format("%d-%02d-01", this.year, this.month));
		periodConsumption = new ArrayList<WaterConsumption>();
		if (periodMeasurements != null && !periodMeasurements.isEmpty()) {
			periodMeasurements.forEach(measurements -> {
				allApartmentMeasurements.put(measurements.getApartmentName(), measurements);
			});
		}
		allApartmentMeasurements.forEach((name, measurements) -> {
			Measurements previous = null;
			if (measurements != null)
				previous = this.measurementsDAO.getPreviousMeasurementsWithoutNulls(measurements.getApartmentId(),
					reportPeriod);
			periodConsumption.add(new WaterConsumption(name, measurements, previous));
		});

		return periodConsumption;
}

	public void setPeriodConsumption(List<WaterConsumption> periodConsumption) {
		this.periodConsumption = periodConsumption;
	}

	public MeasurementsDAO getMeasurementsDAO() {
		return measurementsDAO;
	}

	public void setMeasurementsDAO(MeasurementsDAO measurementsDAO) {
		this.measurementsDAO = measurementsDAO;
	}

	public Report() {
	}

	public Report(MeasurementsDAO measurementsDAO) {
		this.measurementsDAO = measurementsDAO;
		LocalDate latestPeriod = this.measurementsDAO.getLatestPeriod();
		year = latestPeriod.getYear();
		month = latestPeriod.getMonthValue();
	}

}
