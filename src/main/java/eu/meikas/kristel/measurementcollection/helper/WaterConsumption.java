package eu.meikas.kristel.measurementcollection.helper;

import eu.meikas.kristel.measurementcollection.domain.Measurements;

import java.math.BigDecimal;

public class WaterConsumption {
	private String apartmentName;
	private BigDecimal kitchenHotDelta;
	private BigDecimal kitchenColdDelta;
	private BigDecimal bathroomHotDelta;
	private BigDecimal bathroomColdDelta;
	private BigDecimal totalHot;
	private BigDecimal totalCold;
	private BigDecimal totalWater;
	private boolean anyTotalNull;

	public String getApartmentName() {
		return apartmentName;
	}

	public BigDecimal getKitchenHotDelta() {
		return kitchenHotDelta;
	}

	public BigDecimal getKitchenColdDelta() {
		return kitchenColdDelta;
	}

	public BigDecimal getBathroomHotDelta() {
		return bathroomHotDelta;
	}

	public BigDecimal getBathroomColdDelta() {
		return bathroomColdDelta;
	}

	public BigDecimal getTotalHot() {
		return totalHot;
	}

	public BigDecimal getTotalCold() {
		return totalCold;
	}

	public BigDecimal getTotalWater() {
		return totalWater;
	}

	public boolean isAnyTotalNull() {
		return anyTotalNull;
	}

	public WaterConsumption(String name, Measurements current, Measurements previous) {
		apartmentName = name;
		if (current != null && previous != null) {
			if (current.getKitchenHot() != null && previous.getKitchenHot() != null)
				kitchenHotDelta = current.getKitchenHot().subtract(previous.getKitchenHot());
			else if (current.getKitchenHot() != null)
				kitchenHotDelta = current.getKitchenHot();
			if (current.getKitchenCold() != null && previous.getKitchenCold() != null)
				kitchenColdDelta = current.getKitchenCold().subtract(previous.getKitchenCold());
			else if (current.getKitchenCold() != null)
				kitchenColdDelta = current.getKitchenCold();
			if (current.getBathroomHot() != null && previous.getBathroomHot() != null)
				bathroomHotDelta = current.getBathroomHot().subtract(previous.getBathroomHot());
			else if (current.getBathroomHot() != null)
				bathroomHotDelta = current.getBathroomHot();
			if (current.getBathroomCold() != null && previous.getBathroomCold() != null)
				bathroomColdDelta = current.getBathroomCold().subtract(previous.getBathroomCold());
			else if (current.getBathroomCold() != null)
				bathroomColdDelta = current.getBathroomCold();
		}

		if (kitchenHotDelta != null && bathroomHotDelta != null)
			totalHot = kitchenHotDelta.add(bathroomHotDelta);
		else if (kitchenHotDelta != null)
			totalHot = kitchenHotDelta;
		else if (bathroomHotDelta != null)
			totalHot = bathroomHotDelta;

		if (kitchenColdDelta != null && bathroomColdDelta != null)
			totalCold = kitchenColdDelta.add(bathroomColdDelta);
		else if (kitchenColdDelta != null)
			totalCold = kitchenColdDelta;
		else if (bathroomColdDelta != null)
			totalCold = bathroomColdDelta;

		if (totalHot != null && totalCold != null)
			totalWater = totalHot.add(totalCold);
		else if (totalHot != null)
			totalWater = totalHot;
		else if (totalCold != null)
			totalWater = totalCold;

		anyTotalNull = totalHot == null || totalCold == null;
	}
}
