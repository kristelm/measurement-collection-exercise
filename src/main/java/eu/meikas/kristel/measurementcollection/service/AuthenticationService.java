package eu.meikas.kristel.measurementcollection.service;

import eu.meikas.kristel.measurementcollection.dao.ApartmentDAO;
import eu.meikas.kristel.measurementcollection.domain.Apartment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class AuthenticationService implements UserDetailsService {

	@Autowired
	private ApartmentDAO apartmentDAO;

	@Override
	public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
		Apartment apartment = apartmentDAO.getApartmentByName(name);
		Collection<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority("USER"));
		if (apartment.isManager())
			authorities.add(new SimpleGrantedAuthority("MANAGER"));
		UserDetails userDetails = new ApartmentUserDetails(apartment.getUsername(), apartment.getPasshash(),
				apartment.getId(), authorities);
		return userDetails;
	}

}
