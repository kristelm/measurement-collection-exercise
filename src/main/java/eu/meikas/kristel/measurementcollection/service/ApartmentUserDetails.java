package eu.meikas.kristel.measurementcollection.service;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class ApartmentUserDetails extends User {

	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public ApartmentUserDetails(String username, String password, int id, Collection<GrantedAuthority> authorities) {
		super(username, password, true, true, true, true, authorities);
		this.id = id;
	}

	public ApartmentUserDetails(String username, String password, int id, boolean enabled, boolean accountNonExpired,
								boolean credentialsNonExpired, boolean accountNonLocked,
								Collection<GrantedAuthority> authorities) {

		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.id = id;
	}

}
