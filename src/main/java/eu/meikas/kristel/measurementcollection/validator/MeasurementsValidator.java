package eu.meikas.kristel.measurementcollection.validator;


import eu.meikas.kristel.measurementcollection.dao.MeasurementsDAO;
import eu.meikas.kristel.measurementcollection.domain.Measurements;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class MeasurementsValidator implements Validator {

	private final String MEASUREMENTS_ALREADY_ENTERED_FOR_PERIOD = "Measurements have already been entered for this period!";
	private final String VALUE_MUST_BE_SET = "Value must be set!";
	private final String VALUE_CANNOT_BE_LESS = "Value cannot be less than the value of the previous period!";

	MeasurementsDAO measurementsDAO;

	public void setMeasurementsDAO(MeasurementsDAO measurementsDAO) {
		this.measurementsDAO = measurementsDAO;
	}

	@Override
	public boolean supports(Class clazz) {
		return Measurements.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Measurements m = (Measurements) target;
		if (measurementsDAO.hasMeasurementPeriodBeenEntered(m.getApartmentId(), m.getMeasurementPeriod())) {
			errors.rejectValue("year", "measurements.already_entered_for_period", MEASUREMENTS_ALREADY_ENTERED_FOR_PERIOD);
		}

		if (m.getKitchenHot() == null)
			errors.rejectValue("kitchenHot", "measurements.value_must_be_set", "Kitchen - hot water: " + VALUE_MUST_BE_SET);
		if (m.getKitchenCold() == null)
			errors.rejectValue("kitchenCold", "measurements.value_must_be_set", "Kitchen - cold water: " + VALUE_MUST_BE_SET);
		if (m.getBathroomHot() == null)
			errors.rejectValue("bathroomHot", "measurements.value_must_be_set", "Bathroom - hot water: " + VALUE_MUST_BE_SET);
		if (m.getBathroomCold() == null)
			errors.rejectValue("bathroomCold", "measurements.value_must_be_set", "Bathroom - cold water: " + VALUE_MUST_BE_SET);

		Measurements latest = measurementsDAO.getPreviousMeasurementsWithoutNulls(m.getApartmentId(), m.getMeasurementPeriod());
		if (latest.getKitchenHot() != null && m.getKitchenHot().compareTo(latest.getKitchenHot()) < 0)
			errors.rejectValue("kitchenHot", "measurements.value_cannot_be_less", "Kitchen - hot water: " + VALUE_CANNOT_BE_LESS);
		if (latest.getKitchenCold() != null && m.getKitchenCold().compareTo(latest.getKitchenCold()) < 0)
			errors.rejectValue("kitchenCold", "measurements.value_cannot_be_less", "Kitchen - cold water: " + VALUE_CANNOT_BE_LESS);
		if (latest.getBathroomHot() != null && m.getBathroomHot().compareTo(latest.getBathroomHot()) < 0)
			errors.rejectValue("bathroomHot", "measurements.value_cannot_be_less", "Bathroom - hot water: " + VALUE_CANNOT_BE_LESS);
		if (latest.getBathroomCold() != null && m.getBathroomCold().compareTo(latest.getBathroomCold()) < 0)
			errors.rejectValue("bathroomCold", "measurements.value_cannot_be_less", "Bathroom - cold water: " + VALUE_CANNOT_BE_LESS);
	}

}
