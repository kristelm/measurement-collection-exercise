package eu.meikas.kristel.measurementcollection.dao;


import eu.meikas.kristel.measurementcollection.domain.Measurements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

@Service
public class MeasurementsDAO {

	private final String GET_COUNT_BY_APARTMENT_AND_DATE = "SELECT COUNT(*) AS count FROM measurements WHERE " +
			"apartment_id = %d AND measurement_period = '%s'";
	private final String SELECT_PREVIOUS_BY_APARTMENT_AND_DATE = "SELECT * FROM measurements WHERE apartment_id = %d AND " +
			"measurement_period < '%s' ORDER BY measurement_period DESC LIMIT %d";
	private final String GET_PREVIOUS_NOT_NULL_BY_APARTMENT_AND_MEASUREMENT_POINT = "SELECT %s FROM measurements WHERE apartment_id = %d " +
			"AND measurement_period < '%s' AND %s IS NOT NULL ORDER BY %s DESC LIMIT 1"; // mPoint, apartmentId, mPeriod, mPoint, mPoint
	private final String SELECT_ALL_BY_DATE = "SELECT m.*, a.name FROM measurements AS m JOIN apartment AS a ON " +
			"m.apartment_id = a.id WHERE m.measurement_period = '%s'";
	private final String INSERT_NEW_MEASUREMENTS = "INSERT INTO measurements (apartment_id, kitchen_hot, kitchen_cold, " +
			"bathroom_hot, bathroom_cold, measurement_period) VALUES (?, ?, ?, ?, ?, ?)";
	private final String GET_LATEST_PERIOD = "SELECT MAX(measurement_period) AS max FROM measurements";


	@Autowired
	private JdbcTemplate jdbcTemplate;

	public List<Measurements> getPreviousMeasurementsByApartment(int apartmentId) {
		LocalDate measurementPeriod = LocalDate.now().withDayOfMonth(1);
		return getPreviousMeasurementsByApartment(apartmentId, measurementPeriod, 1);
	}

	public List<Measurements> getPreviousMeasurementsByApartment(int apartmentId, LocalDate measurementPeriod) {
		return getPreviousMeasurementsByApartment(apartmentId, measurementPeriod, 1);
	}

	public List<Measurements> getPreviousMeasurementsByApartment(int apartmentId, LocalDate measurementPeriod,
																 int rowLimit) {
		List<Measurements> query = jdbcTemplate.query(String.format(SELECT_PREVIOUS_BY_APARTMENT_AND_DATE, apartmentId,
				measurementPeriod, rowLimit), new RowMapper<Measurements>() {
					@Override
					public Measurements mapRow(ResultSet rs, int rowNum) throws SQLException {
						Measurements meas = new Measurements();
						meas.setId(rs.getInt("id"));
						meas.setApartmentId(rs.getInt("apartment_id"));
						meas.setKitchenHot(rs.getBigDecimal("kitchen_hot"));
						meas.setKitchenCold(rs.getBigDecimal("kitchen_cold"));
						meas.setBathroomHot(rs.getBigDecimal("bathroom_hot"));
						meas.setBathroomCold(rs.getBigDecimal("bathroom_cold"));
						meas.setMeasurementPeriod(rs.getDate("measurement_period").toLocalDate());
						return meas;
					}
				});
		return query;
	}

	public boolean hasMeasurementPeriodBeenEntered(int apartmentId, LocalDate measurementPeriod) {
		Integer count = jdbcTemplate.queryForObject(String.format(GET_COUNT_BY_APARTMENT_AND_DATE, apartmentId,
				measurementPeriod.withDayOfMonth(1).toString()), new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getInt("count");
			}
		});
		return count == null || count > 0;
	}

	public Measurements getPreviousMeasurementsWithoutNulls(int apartmentId, LocalDate measurementPeriod) {
		Measurements result = null;
		List<Measurements> previousMeasurements = getPreviousMeasurementsByApartment(apartmentId, measurementPeriod);
		if (previousMeasurements != null && !previousMeasurements.isEmpty()) {
			result = previousMeasurements.get(0);

			if (result.getKitchenHot() == null)
				result.setKitchenHot(getPreviousNotNullByApartmentAndMeasurementPoint(apartmentId, "kitchen_hot",
						measurementPeriod));

			if (result.getKitchenCold() == null)
				result.setKitchenCold(getPreviousNotNullByApartmentAndMeasurementPoint(apartmentId, "kitchen_cold",
						measurementPeriod));

			if (result.getBathroomHot() == null)
				result.setBathroomHot(getPreviousNotNullByApartmentAndMeasurementPoint(apartmentId, "bathroom_hot",
						measurementPeriod));

			if (result.getBathroomCold() == null)
				result.setBathroomCold(getPreviousNotNullByApartmentAndMeasurementPoint(apartmentId, "bathroom_cold",
						measurementPeriod));
		}

		if (result == null)
			result = new Measurements();
		return result;
	}

	public BigDecimal getPreviousNotNullByApartmentAndMeasurementPoint(int apartmentId, final String mPoint, LocalDate mPeriod) {
		List<BigDecimal> result = jdbcTemplate.query(String.format(GET_PREVIOUS_NOT_NULL_BY_APARTMENT_AND_MEASUREMENT_POINT,
				mPoint, apartmentId, mPeriod, mPoint, mPoint), new RowMapper<BigDecimal>() {
					@Override
					public BigDecimal mapRow(ResultSet rs, int rowNum) throws SQLException {
						return rs.getBigDecimal(mPoint);
					}
				});
		if (result.isEmpty())
			return null;
		else
			return result.get(0);
	}

	public List<Measurements> getAllMeasurementsByPeriod(int year, int month) {
		String measurementPeriod = String.format("%d-%02d-01", year, month); // ensure month is in two digit format
		List<Measurements> query = jdbcTemplate.query(String.format(SELECT_ALL_BY_DATE, measurementPeriod),
				new RowMapper<Measurements>() {
					@Override
					public Measurements mapRow(ResultSet rs, int rowNum) throws SQLException {
						Measurements meas = new Measurements();
						meas.setId(rs.getInt("id"));
						meas.setApartmentId(rs.getInt("apartment_id"));
						meas.setKitchenHot(rs.getBigDecimal("kitchen_hot"));
						meas.setKitchenCold(rs.getBigDecimal("kitchen_cold"));
						meas.setBathroomHot(rs.getBigDecimal("bathroom_hot"));
						meas.setBathroomCold(rs.getBigDecimal("bathroom_cold"));
						meas.setMeasurementPeriod(rs.getDate("measurement_period").toLocalDate());
						meas.setApartmentName(rs.getString("name"));
						return meas;
					}
				});
		return query;
	}

	public int insertNewMeasurements(Measurements m) {
		int rows = jdbcTemplate.update(INSERT_NEW_MEASUREMENTS, m.getApartmentId(), m.getKitchenHot(), m.getKitchenCold(),
				m.getBathroomHot(), m.getBathroomCold(), m.getMeasurementPeriod().withDayOfMonth(1));
		return rows;
	}

	public LocalDate getLatestPeriod() {
		LocalDate latestPeriod = jdbcTemplate.queryForObject(GET_LATEST_PERIOD, new RowMapper<LocalDate>() {
			@Override
			public LocalDate mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getDate("max").toLocalDate();
			}
		});
			return latestPeriod;
	}
}
