package eu.meikas.kristel.measurementcollection.dao;


import eu.meikas.kristel.measurementcollection.domain.Apartment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ApartmentDAO {

	private final String SELECT_ALL_BY_NAME = "SELECT * FROM apartment WHERE name = '%s'";

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public List<String> getAllApartmentNames() {
		List<String> query = jdbcTemplate.query("SELECT name FROM apartment", (rs, rowNum) -> rs.getString("name"));
		return query;
	}

	public Apartment getApartmentByName(String name) {
		Apartment apartment = jdbcTemplate.queryForObject(String.format(SELECT_ALL_BY_NAME, name), (rs, rowNum) -> {
			Apartment ap = new Apartment();
			ap.setId(rs.getInt("id"));
			ap.setSalt(rs.getString("salt"));
			ap.setPasshash(rs.getString("passhash"));
			ap.setUsername(rs.getString("name"));
			ap.setManager(rs.getBoolean("manager"));
			return ap;
		});
		return apartment;
	}

	/*public boolean authenticate(String name, String password) {
		List<Apartment> query = jdbcTemplate.query(String.format(SELECT_ALL_BY_NAME, name),
				new RowMapper<Apartment>() {
					@Override
					public Apartment mapRow(ResultSet rs, int rowNum) throws SQLException {
						Apartment ap = new Apartment();
						ap.setId(rs.getInt("id"));
						ap.setSalt(rs.getString("salt"));
						ap.setPasshash(rs.getString("passhash"));
						ap.setUsername(rs.getString("name"));
						ap.setManager(rs.getBoolean("manager"));

						String localPasshash = MD5Util.getMD5(password + ap.getSalt());
						ap.setAuthenticated(ap.getPasshash().equals(localPasshash));

						return ap;
					}
				});

		return query.size() == 1 && query.get(0).isAuthenticated();
	}*/

}
