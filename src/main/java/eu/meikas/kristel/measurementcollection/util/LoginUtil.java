package eu.meikas.kristel.measurementcollection.util;

import eu.meikas.kristel.measurementcollection.service.ApartmentUserDetails;
import org.springframework.security.core.context.SecurityContextHolder;

public class LoginUtil {

	public static ApartmentUserDetails getLoggedInApartmentDetails() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof ApartmentUserDetails) {
			return (ApartmentUserDetails) principal;
		} else {
			return null;
		}
	}
}
