-- Database: measurement_collection

-- DROP DATABASE measurement_collection;

CREATE DATABASE measurement_collection
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
	
-- SCHEMA: public

-- DROP SCHEMA public ;

CREATE SCHEMA public
    AUTHORIZATION postgres;

COMMENT ON SCHEMA public
    IS 'standard public schema';

GRANT ALL ON SCHEMA public TO postgres;

GRANT ALL ON SCHEMA public TO PUBLIC;


CREATE SEQUENCE public."apartment_ID_seq"
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public."apartment_ID_seq"
    OWNER TO postgres;

CREATE SEQUENCE public."measurements_ID_seq"
    INCREMENT 1
    START 238
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public."measurements_ID_seq"
    OWNER TO postgres;

-- Table: public.apartment

-- DROP TABLE public.apartment;

CREATE TABLE public.apartment
(
    id integer NOT NULL DEFAULT nextval('"apartment_ID_seq"'::regclass),
    name character varying NOT NULL,
    passhash character varying NOT NULL,
    salt character varying,
    manager boolean NOT NULL DEFAULT false,
    CONSTRAINT apartment_pkey PRIMARY KEY (id),
    CONSTRAINT name_unique UNIQUE (name)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.apartment
    OWNER to postgres;

-- Table: public.measurements

-- DROP TABLE public.measurements;

CREATE TABLE public.measurements
(
    id integer NOT NULL DEFAULT nextval('"measurements_ID_seq"'::regclass),
    apartment_id integer NOT NULL,
    kitchen_hot numeric,
    kitchen_cold numeric,
    bathroom_hot numeric,
    bathroom_cold numeric,
    measurement_period date,
    CONSTRAINT measurements_pkey PRIMARY KEY (id),
    CONSTRAINT "measurements_apartmentID_fkey" FOREIGN KEY (apartment_id)
        REFERENCES public.apartment (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.measurements
    OWNER to postgres;
