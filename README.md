# README #

MeasurementCollectionExercise is a web application for collecting monthly water usage measurements in an apartment building and allows managers to view water usage reports.

### Technologies used ###
- Java 8
- PostgreSQL
- Maven
- Spring Framework (Boot, Security, Web etc)
    - Bundled Tomcat
- Thymeleaf


### How do I get set up in IntelliJ IDEA? ###

###### Importing the project ######
- Create new project from version control (Git) url https://bitbucket.org/kristelm/measurement-collection-exercise
- Set project up as Maven project
- Open File > Project Structure and set project SDK to 1.8

###### Creating the database (originally a PostgreSQL database - if using another, modify SQL accordingly) ######
- Locate the folder sql under src/main/resources
- Run the SQL in setup.sql first to create the database, schema, tables etc.
- Insert user (apartment) data from insert_data_apartment.sql
     - Inserts 25 apartments with password hashes (passwords are 01, 02 etc according to the username)
- _[Optional]_ Insert measurement data for testing purposes from insert_data_measurements.sql
- Configure the database connection in the application.properties file

###### Deploying the project (executable JAR) ######
- The application server is bundled in Spring Boot
- Run maven:clean and maven:install to create the deployable JAR file
- Run the JAR file with java -jar filename.jar